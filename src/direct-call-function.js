
/**
 * Responds to any HTTP request that can provide a "message" field in the body.
 *
 * @param {!Object} req Cloud Function request context.
 * @param {!Object} res Cloud Function response context.
 */
exports.helloWorld = function helloWorld(req, res) {
    // Example input: {"message": "Hello!"}
    if (req.body.messages === undefined) {
        // This is an error case, as "message" is required.
        res.status(400).send('No message defined!');
    } else {
        // Everything is okay.
        // Note the pub sub format of messages array and data for the message content
        const a = Buffer.from(req.body.messages[0].data, 'base64').toString();
        console.log(a);
        res.status(200).send('Success: ' + a);
    }
};