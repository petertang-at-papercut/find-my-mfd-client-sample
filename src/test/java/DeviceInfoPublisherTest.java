import com.google.auth.oauth2.GoogleCredentials;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;

public class DeviceInfoPublisherTest {

    public void sendMessage(String url) throws Exception {
        GoogleCredentials credential = GoogleCredentials.fromStream(
                Files.newInputStream(Paths.get("/Users/petert/Downloads/Test Project-05e7e4bdbafd.json")))
                .createScoped(Collections.singleton("https://www.googleapis.com/auth/pubsub"));

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        // Need to review the proper way to get this token
        credential.refreshAccessToken();
        credential.refresh();
        headers.add("Authorization", "Bearer " + credential.getAccessToken().getTokenValue());
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>("{" +
                "\"messages\": [ {\"data\": \"SGVsbG8gQ2xvdWQgUHViL1N1YiEgSGVyZSBpcyBteSBtZXNzYWdlIQ\"} ] \n" +
                "}", headers);

        ResponseEntity<String> response = restTemplate.exchange(url,
                HttpMethod.POST, entity, String.class);

        System.out.println(response.getStatusCode());
        System.out.println(response.getBody());
    }

    @Test
    public void sendMessage_to_pubsub() {
        try {
            sendMessage("https://pubsub.googleapis.com/v1/projects/polished-vault-180401/topics/tester:publish");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void sendMessage_to_function() {
        try {
            sendMessage("https://us-central1-polished-vault-180401.cloudfunctions.net/peter-test");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
